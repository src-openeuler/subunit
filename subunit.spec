Name:                subunit
Version:             1.4.2
Release:             3
Summary:             C bindings for subunit
License:             Apache-2.0 OR BSD-3-Clause
URL:                 https://launchpad.net/subunit
Source0:             https://launchpad.net/subunit/trunk/%{version}/+download/%{name}-%{version}.tar.gz

BuildRequires:       gcc-c++
BuildRequires:       libtool
BuildRequires:       make
BuildRequires:       perl-generators
BuildRequires:       perl(ExtUtils::MakeMaker)
BuildRequires:       pkgconfig
BuildRequires:       pkgconfig(cppunit)
BuildRequires:       python3-devel
BuildRequires:       python3-pip
BuildRequires:       python3-wheel
BuildRequires:       python3-testtools >= 1.8.0
BuildRequires:       python3-testscenarios
BuildRequires:       pkgconfig(check)
%description
Subunit C bindings.  See the python-subunit package for test processing
functionality.

%package devel
Summary:             Header files for developing C applications that use subunit
Requires:            %{name}%{?_isa} = %{version}-%{release}
%description devel
Header files and libraries for developing C applications that use subunit.

%package cppunit
Summary:             Subunit integration into cppunit
Requires:            %{name}%{?_isa} = %{version}-%{release}
%description cppunit
Subunit integration into cppunit.

%package cppunit-devel
Summary:             Header files for applications that use cppunit and subunit
Requires:            %{name}-cppunit%{?_isa} = %{version}-%{release}
Requires:            %{name}-devel%{?_isa} = %{version}-%{release} cppunit-devel%{?_isa}
%description cppunit-devel
Header files and libraries for developing applications that use cppunit
and subunit.

%package perl
Summary:             Perl bindings for subunit
BuildArch:           noarch
%description perl
Subunit perl bindings.  See the python-subunit package for test
processing functionality.

%package shell
Summary:             Shell bindings for subunit
BuildArch:           noarch
%description shell
Subunit shell bindings.  See the python-subunit package for test
processing functionality.

%package -n python3-%{name}
# The bundled iso8601 library is MIT licensed
License:             (Apache-2.0 OR BSD-3-Clause) AND MIT
Summary:             Streaming protocol for test results
BuildArch:           noarch
Provides:            bundled(python3-iso8601) = 0.1.4
%description -n python3-%{name}
Subunit is a streaming protocol for test results.  The protocol is a
binary encoding that is easily generated and parsed.  By design all the
components of the protocol conceptually fit into the xUnit TestCase ->
TestResult interaction.
Subunit comes with command line filters to process a subunit stream and
language bindings for python, C, C++ and shell.  Bindings are easy to
write for other languages.
A number of useful things can be done easily with subunit:
- Test aggregation: Tests run separately can be combined and then
  reported/displayed together.  For instance, tests from different
  languages can be shown as a seamless whole.
- Test archiving: A test run may be recorded and replayed later.
- Test isolation: Tests that may crash or otherwise interact badly with
  each other can be run separately and then aggregated, rather than
  interfering with each other.
- Grid testing: subunit can act as the necessary serialization and
  deserialization to get test runs on distributed machines to be
  reported in real time.

%package -n python3-%{name}-test
Summary:             Test code for the python 3 subunit bindings
BuildArch:           noarch
Requires:            python3-%{name} = %{version}-%{release} %{name}-filters = %{version}-%{release}

%description -n python3-%{name}-test
%{summary}.

%package filters
Summary:             Command line filters for processing subunit streams
BuildArch:           noarch
Requires:            python3-%{name} = %{version}-%{release} python3-gobject gtk3 >= 3.20
Requires:            libnotify >= 0.7.7 python3-junitxml
%description filters
Command line filters for processing subunit streams.

%package static
Summary:             Static C library for subunit
Requires:            %{name}-devel%{?_isa} = %{version}-%{release}
%description static
Subunit C bindings in a static library, for building statically linked
test cases.

%prep
%autosetup -n %{name}-%{version} -p1
fixtimestamp() {
  touch -r $1.orig $1
  rm $1.orig
}
sed "/^tests_LDADD/ilibcppunit_subunit_la_LIBADD = -lcppunit libsubunit.la\n" \
    -i Makefile.am
for fil in $(grep -Frl "%{_bindir}/env python"); do
  sed -i.orig 's,%{_bindir}/env python3,%{_bindir}/python3,' $fil
  fixtimestamp $fil
done

%build
autoreconf -fi
export INSTALLDIRS=perl
 
# Build for python3
export PYTHON=%{_bindir}/python3
%configure --enable-shared --enable-static
%disable_rpath
%make_build
%pyproject_build

%install
%pyproject_install
chmod 0755 %{buildroot}%{python3_sitelib}/%{name}/run.py
chmod 0755 %{buildroot}%{python3_sitelib}/%{name}/tests/sample-script.py
chmod 0755 %{buildroot}%{python3_sitelib}/%{name}/tests/sample-two-script.py

%make_install
%delete_la

mkdir -p %{buildroot}%{_sysconfdir}/profile.d
cp -p shell/share/%{name}.sh %{buildroot}%{_sysconfdir}/profile.d

mkdir -p %{buildroot}%{perl_vendorlib}
mv %{buildroot}%{perl_privlib}/Subunit* %{buildroot}%{perl_vendorlib}
rm -fr %{buildroot}%{perl_archlib}

chmod 0755 %{buildroot}%{_bindir}/subunit-diff
chmod 0755 %{buildroot}%{python3_sitelib}/%{name}/filter_scripts/*.py
chmod 0644 %{buildroot}%{python3_sitelib}/%{name}/filter_scripts/__init__.py
chmod 0755 %{buildroot}%{python3_sitelib}/%{name}/tests/sample-script.py
chmod 0755 %{buildroot}%{python3_sitelib}/%{name}/tests/sample-two-script.py
touch -r c/include/%{name}/child.h %{buildroot}%{_includedir}/%{name}/child.h
touch -r c++/SubunitTestProgressListener.h \
      %{buildroot}%{_includedir}/%{name}/SubunitTestProgressListener.h
touch -r perl/subunit-diff %{buildroot}%{_bindir}/subunit-diff

%check
export LD_LIBRARY_PATH=$PWD/.libs
export PYTHON=%{__python3}
make check
PYTHONPATH=%{buildroot}%{python3_sitelib} %{__python3} -c "import subunit.iso8601"

%files
%doc NEWS README.rst
%license Apache-2.0 BSD COPYING
%{_libdir}/lib%{name}.so.*

%files devel
%doc c/README
%dir %{_includedir}/%{name}/
%{_includedir}/%{name}/child.h
%{_libdir}/lib%{name}.so
%{_libdir}/pkgconfig/lib%{name}.pc

%files cppunit
%{_libdir}/libcppunit_%{name}.so.*

%files cppunit-devel
%doc c++/README
%{_includedir}/%{name}/SubunitTestProgressListener.h
%{_libdir}/libcppunit_%{name}.so
%{_libdir}/pkgconfig/libcppunit_%{name}.pc

%files perl
%license Apache-2.0 BSD COPYING
%{_bindir}/%{name}-diff
%{perl_vendorlib}/*

%files shell
%doc shell/README
%license Apache-2.0 BSD COPYING
%config(noreplace) %{_sysconfdir}/profile.d/%{name}.sh

%files -n python3-%{name}
%license Apache-2.0 BSD COPYING
%{python3_sitelib}/%{name}/
%{python3_sitelib}/python_%{name}-%{version}.dist-info/
%exclude %{python3_sitelib}/%{name}/tests/

%files -n python3-%{name}-test
%{python3_sitelib}/%{name}/tests/

%files static
%{_libdir}/*.a

%files filters
%{_bindir}/subunit-1to2
%{_bindir}/subunit-2to1
%{_bindir}/subunit-filter
%{_bindir}/subunit-ls
%{_bindir}/subunit-notify
%{_bindir}/subunit-output
%{_bindir}/subunit-stats
%{_bindir}/subunit-tags
%{_bindir}/subunit2csv
%{_bindir}/subunit2disk
%{_bindir}/subunit2gtk
%{_bindir}/subunit2junitxml
%{_bindir}/subunit2pyunit
%{_bindir}/tap2subunit

%changelog
* Thu Jan 16 2025 Funda Wang <fundawang@yeah.net> - 1.4.2-3
- drop useless perl(:MODULE_COMPAT) requirement
- cleanup spec

* Mon Jul 22 2024 yuanlipeng <yuanlipeng2@huawei.com> - 1.4.2-2
- Fix build failure due to automake upgrade

* Tue Sep 05 2023 wangkai <13474090681@163.com> - 1.4.2-1
- Update to 1.4.2
- Use pyproject to compile package
- Modify iso8601 to bundle

* Fri Aug 04 2023 yaoxin <yao_xin001@hoperun.com> - 1.4.0-3
- Fix test failure caused by python update to 3.11

* Thu Jul 07 2022 yaoxin <yaoxin30@h-partners.com> - 1.4.0-2
- Resolve the compilation fails, due to python-testtools update to 2.5.0

* Mon Jul 12 2021 huangtianhua <huangtianhua@huawei.com> - 1.4.0-1
- Upgrade to 1.4.0 and drop the patches which have been upstreamed

* Mon Feb 22 2021 wangxiyuan <wangxiyuan1007@gmail.com> - 1.3.0-15
- CleanUp python2 residual content and backport a python3 known issue.

* Wed Feb 03 2021 maminjie <maminjie1@huawei.com> - 1.3.0-14
- Port to python-iso8601 0.1.12

* Tue Aug 11 2020 yanan li <liyanan032@huawei.com> - 1.3.0-13
- Remove python2-subunit subpackage 

* Wed Jun 24 2020 sunguoshuai <sunguoshuai@huawei.com> - 1.3.0-12
- Update to 1.3.0-12

* Thu Dec 5 2019 wanjiankang <wanjiankang@huawei.com> - 1.3.0-3
- Initial RPM
